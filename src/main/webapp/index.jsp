<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<c:set var="page" value="${sessionScope.contentPage}"/>
<c:set var="fileName" value="home" />
<c:if test="${page != null && page != ''}">
    <c:set var="fileName" value="${sessionScope.contentPage}" />
</c:if>
<html>
    <head>
        <title>Main Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <jsp:include page="WEB-INF/includes/bootstrap_deps.jsp" />
        <link type="text/css" href="public/css/main.css" rel="stylesheet">
    </head>
    <body id="webpage">
        <jsp:include page="WEB-INF/includes/navbar.jsp" />
        <div id="main-wrapper" class="main-wrapper">
            <div class="content-page">
                <c:if test="${requestScope.errorMsg != null}">
                    <p class="alert alert-danger"><c:out value="${requestScope.errorMsg}"/></p>
                </c:if>
                <jsp:include page="WEB-INF/${fileName}.jsp" />
            </div>
        </div>
        <jsp:include page="WEB-INF/includes/footer.jsp" />                  
        <div id="float-login" >
            <h3 id="login-msg"></h3>
            <form id="login_form">   
                <label for="email" class="text-formatting">Email address</label>
                <input type="email" class="form-control" id="loginemail" name="email" placeholder="Enter email" required>
                <label for="password" class="text-formatting">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" >
                <a href="forgottenpw">Have forgot your password?</a>
                <button id="send-login-btn" class="btn btn-success btn-lg">Login</button>
                <a class="btn btn-secondary btn-lg" href="registration">Registrate</a>
                <button id="close-login-form" class="btn btn-danger btn-lg">Cancel</button>
            </form>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" id="close-product-details-button" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="images p-3">                                                       
                                    <div class="text-center p-4"> <img id="modal-product-image" width="400" src="" /> </div>                                                    </div>
                            </div>
                            <hr id="vertical-line">
                            <div class="col-md-6">
                                <div class="product-details-container p-4">
                                    <span class="text-uppercase text-muted" id="modal-product-manufacturer"></span>
                                    <h5 class="text-uppercase" id="modal-product-name"></h5>
                                    <p id="modal-product-details"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <span id="modal-product-price"></span>   
                        <button class="btn btn-warning" id="add-product-button" onclick="addToCartFromModal()">Add</button>
                    </div>
                </div>
            </div>
        </div>   
        <script type="text/javascript" src="public/script/main.js"></script>
        <script type="text/javascript" src="public/script/ajax.js"></script>
        <script src="public/script/detailajax.js" type="text/javascript"></script>
    </body>

</html>