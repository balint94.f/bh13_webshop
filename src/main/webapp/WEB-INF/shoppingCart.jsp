<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="cart" value="${sessionScope.shoppingCart.items}" />
<c:set var="totalPrice" value="${requestScope.totalPrice}" />
<div class="shoppingcart-page ">
    <c:if test="${empty cart}">
        <div class="cart-warning-container">
            <h1 id="cart-warning" class="">Your cart is empty!</h1>
            <a class="btn btn-danger" href="product"><- Back to products</a>
        </div>
    </c:if>
    <c:if test="${not empty cart}">
        <div class="container products-container">

            <div class="row">
                <div class="col-lg-9">
                    <div class="row">

                        <c:forEach var="product" items="${cart}">
                            <div class="col-lg-4 col-md-6 mb-4">
                                <div class="card h-100 text-dark">
                                    <a href=""><img class="card-img-top" onclick="showDetails(${product.item.productId})" data-toggle="modal" data-target="#myModal" src="<c:out value="${product.item.imgpath}" />"
                                                     alt=""></a>
                                    <div class="card-body">
                                        <h4 class="card-title">
                                            <c:out value="${product.item.name}" />
                                        </h4>
                                        <h5>
                                            <c:out value="${product.item.price}" />
                                            $
                                        </h5>
                                        <p class="card-text text-dark">
                                            <c:out value="${product.item.manufacturer}" />
                                        </p>
                                        <p class="card-text text-dark">Quantity:
                                            <c:out value="${product.quantity}" />
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-danger" onclick="removeFromCart(${product.id})" style="color:#fff">Remove</button>
                                        <button class="btn btn-primary" onclick="showDetails(${product.item.productId})" data-toggle="modal" data-target="#myModal">Details</button>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>

                    <!-- /.row -->
                </div>
                <!-- /.col-lg-9 -->
            </div>
            <!-- /.row -->
        </div>
        <div class="text-center">
            <a class="btn btn-primary" href="product"><- Back to products</a>
            <c:if test="${sessionScope.user == null}">
                <button class="btn btn-danger" onclick="showLoginForm()" style="color:#fff">Login</button>
                <p class="color-red">You have to login to send your order.</p>
            </c:if>
            <c:if test="${sessionScope.user != null}">
                <a class="btn btn-success " href="cashier">Checkout</a>
            </c:if>

            <c:if test="${totalPrice != null}">
                <p class="text-light h5">Total Price: <span class="text-warning h1"><c:out value="${totalPrice}" /></span></p>
                </c:if>
        </div>
    </c:if>




</div>