<%@ taglib prefix =  "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="registration-page">
    <h1>Registration Page</h1>
    <c:if test="${not empty param.msg}">
        <div class="alert alert-danger" role="alert">
            <c:out value="${param.msg}" />
        </div>
    </c:if>
    <form action="registration" method="POST" enctype="application/x-www-form-urlencoded">
        <label for="firstName">First name</label>
        <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Enter your first name" required="" >
        <label for="lastName">Last name</label>
        <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Enter your last name" required="">
        <label for="email">Email address</label>
        <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email address" aria-describedby="emailHelp" required="">
        <label for="pwd1">Password</label>
        <input type="password" class="form-control" name="pwd1" id="pwd1" placeholder="Enter your password" aria-describedby="pwd1" required="">
        <label for="pwd2">Confirm password</label>
        <input type="password" class="form-control" name="pwd2" id="pwd2" placeholder="Please confirm your password" aria-describedby="pwd1" required=""> <br>
        <button type="submit" class="btn btn-success btn-lg">Submit</button>
    </form>
      
</div>