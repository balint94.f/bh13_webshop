<%@ taglib prefix =  "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="container-fluid login-page">
    <h1 >Login</h1> <br>
    <c:if test="${not empty param.ok && param.ok == false}">
        <h5 class="text-danger">Invalid username or password!</h5>
    </c:if>
    <form action="login" method="POST" id="login_form">   
        <div class="form-group">
            <label for="email" class="text-formatting">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="">
        </div>
        <div class="form-group">
            <label for="password" class="text-formatting">Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" required="">
        </div>
        <button type="submit" class="btn btn-success btn-lg">Login</button>
        <a class="btn btn-secondary btn-lg" href="registration">Registrate</a>
    </form>
    <a class="nav-item nav-link text-light" href="forgottenpw">Have you forgot your password?</a>
</div>