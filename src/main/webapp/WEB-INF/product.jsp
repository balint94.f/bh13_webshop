<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix =  "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link href="public/css/productDetail.css" rel="stylesheet" type="text/css"/>

<c:set var="products" value="${requestScope['products']}" />
<div class="container product-page">
    <c:if test="${requestScope.message != null || requestScope.message != ''}">
        <div>
            <p class="text-danger"><c:out value="${requestScope.message}" /></p>
        </div>
    </c:if>
    <form action="product" method="GET" id="search_form">
        <div class="d-flex justify-content-around" id="search-group">
            <select class="form-select" id="product-select" name="searchType" aria-label="Default select example">
                <option value="name">Name</option>
                <option value="manufacturer">Manufacturer</option>
            </select>
            <div class="input-group">
                <div class="form-outline">
                    <input type="search" id="form1" name="search" class="form-control" />
                </div>
                <button type="submit" class="btn" id="search-button">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </form>

    <div class="container-fluid">
        <div class="row mb-5 mt-5">  
            <c:if test="${products!=null && fn:length(products)>= 1}">
                <c:forEach var="product" items="${products}">
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-top">
                                <img class="card-img-top" onclick="showDetails(${product.productId})" data-toggle="modal" data-target="#myModal"  src=<c:out value="${product.imgpath}"/> />
                                <button class="btn" id="show-product-details" onclick="showDetails(${product.productId})" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-info"></i>
                                </button>
                            </div>
                            <div class="card-body text-center">
                                <p class="card-title"><c:out value="${product.name}"/></p> 
                                <p class="card-manufacturer"><c:out value="${product.manufacturer}"/></p>
                                <p class="card-price"><c:out value="${product.price}"/> $</p> 
                            </div>
                            <button class="btn" id="add-to-cart"  onclick="addToCart(${product.productId})">
                                <i class="fa fa-shopping-cart"></i>
                            </button>
                        </div>
                            </div>   
                </c:forEach>
            </c:if>

        </div>  
    </div>
</div>