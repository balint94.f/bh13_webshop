<%@page contentType="text/html" pageEncoding="UTF-8"%>

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  margin: 0;
  background: white;
}

html {
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

.column {
  float: left;
  width: 25%;
  margin-bottom: 16px;
  padding: 0 8px;
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  margin: 8px;
}

.about-section {
  padding: 50px;
  text-align: center;
  background-color: #474e5d;
  color: white;
}

.container {
  padding: 0 16px;
}

.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}
.main-title {
    color: #E9D23B;
    font-family: 'Saira Stencil One', cursive;
    font-size: 3.5rem;
    text-align:center;
}

.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}

@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}
</style>
</head>
<body>
    <div style="background-color: white; min-height: 75vh;">
<div class="about-section">
    <h1 class="main-title">About Us</h1>
  
    <p> Welcome to Gun Store webshop, your number one source for all airsoft weapons, accesories .
        We're dedicated to giving you the very best of service, with a focus on quality.
    </p>
    <p>Founded 2021 Gun Store has come a long way from its beginnings in Budapest.
        We now serve customers all over Hungary, and are thrilled that we're able to turn our passion into our own website.
    </p>
    <p>
        We hope you enjoy our products as much as we enjoy offering them to you. 
        If you have any questions or comments, please don't hesitate to contact us.
    </p>
</div>

<h2 class="main-title mt-5">Our Team</h2>
<div class="row" style="width: 85%; margin: 0 auto;">
  <div class="column">
    <div class="card">
      <img src="public/img/prof.jpg" alt="Tamas" style="width:auto">
      <div class="container">
        <h2>Balaton Tamás</h2>
        <p class="title">Developer</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>tamas@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card">
      <img src="public/img/balint.jpg" alt="Balint" style="width:auto">
      <div class="container">
        <h2>Fajka Bálint</h2>
        <p class="title">Developer</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>balint@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>
  
  <div class="column">
    <div class="card">
      <img src="public/img/profpic.jpg" alt="Adam" style="width:auto">
      <div class="container">
        <h2>Tálas Ádám</h2>
        <p class="title">Developer</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>adam@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>
    
    <div class="column">
    <div class="card">
      <img src="public/img/kristofpic.jpg" alt="John" style="width:auto">
      <div class="container">
        <h2>Komiszár Kristóf</h2>
        <p class="title">Developer</p>
        <p>Some text that describes me lorem ipsum ipsum lorem.</p>
        <p>kristof@example.com</p>
        <p><button class="button">Contact</button></p>
      </div>
    </div>
  </div>
</div>
</div>
</body>



<!--<div class="container aboutus-page">
    <h1> About Us</h1>

    <p> Welcome to Gun Store webshop, your number one source for all airsoft weapons, accesories .
        We're dedicated to giving you the very best of service, with a focus on quality.
    </p>
    <p>Founded 2021 Gun Store has come a long way from its beginnings in Budapest.
        We now serve customers all over Hungary, and are thrilled that we're able to turn our passion into our own website.
    </p>
    <p>
        We hope you enjoy our products as much as we enjoy offering them to you. 
        If you have any questions or comments, please don't hesitate to contact us.
    </p>
    <table style="width: 100%">
  <tr>
    <th>Tamas Képe</th>
    <th>Bálint Képe</th>
    <th>Kristóf Képe</th>
    <th>Ádám Képe</th>
  </tr>
  <tr>
    <td>Tamás, Developer</td>
    <td>Bálint, Developer</td>
    <td>Kristóf, Developer</td>S
    <td>Ádám, Developer</td>
  </tr>
</table>

</div>-->
