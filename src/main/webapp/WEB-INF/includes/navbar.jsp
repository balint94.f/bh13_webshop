<%@ taglib prefix =  "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="cart" value="${sessionScope.shoppingCart}" />
<nav class="navbar navbar-expand-md navbar-light ">
    <a id="logo" class="navbar-brand " href="home">Airsoft Store</a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div id="navbar" class="navbar-nav">
            <a href="home" class="nav-item nav-link">Home</a>
            <a href="product" class="nav-item nav-link">Products</a>
            <a href="aboutus" class="nav-item nav-link">About Us</a>
            <a href="faq" class="nav-item nav-link">FAQ</a>
            <a href="contact" class="nav-item nav-link">Contact Us</a>
        </div>
        <div class="navbar-nav ml-auto">
            <a id="cart" class="btn btn-outline-secondary" href="shoppingCart">
                <span id="cart-size" class="text-light">
                    <c:if test="${cart.numberOfItems > 0}">
                        <c:out value="${cart.numberOfItems}" />
                    </c:if>
                </span>
                <i class="fa fa-shopping-cart fa_custom"></i>
            </a>
            <c:if test="${sessionScope.user != null}">
                <a class="btn btn-outline-secondary" href="changepw" style="color:#fff"><i class="fa" aria-hidden="true"></i>Change Password</a>
                <!--fa-user-circle-o-->
                <form method="post" action="logout">
                    <button class="btn btn-danger">Logout</button>
                </form>
            </c:if>
            <c:if test="${sessionScope.user == null}">
                <button class="btn btn-success" onclick="showLoginForm()" style="color:#fff">Login</button>
            </c:if>
        </div>
    </div>
</nav>

