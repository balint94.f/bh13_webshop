<%@ taglib prefix =  "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="container login-page">
    <h1 class="text-formatting">Change Password</h1> <br>
    <c:if test="${not empty param.msg}">
        <h5 class="text-warning"><c:out value="${param.msg}" /></h5>
    </c:if>
    <form action="changepw" method="POST">   
        <div class="form-group">
            <label for="oldPassword" class="text-formatting">Old Password</label>
            <input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="Enter old password">
            <label for="newPassword" class="text-formatting">New Password</label>
            <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="Enter new password">
            <label for="confirmNewPassword" class="text-formatting">Confirm New Password</label>
            <input type="password" class="form-control" id="confirmNewPassword" name="confirmNewPassword" placeholder="Confirm new password">
        </div>
        <button type ="submit" class="button" >Submit</button>

    </form>
</div>
