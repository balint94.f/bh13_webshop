<%@ taglib prefix =  "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="container login-page">
    <h1 class="text-formatting">Forgotten Password</h1> <br>
    <form action="forgottenpw" method="POST">   
        <div class="form-group">
            <label for="email" class="text-formatting">Please provide your email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address"> <br>
            <p> You will receive an email with your new randomly generated password, which you can change anytime after you logged in.</p>
        </div>
        <button type ="submit" class="button" >Send</button>
        
    </form>
</div>
