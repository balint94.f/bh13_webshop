<%@page import="hu.bh.bh13.webshop.cart.ShoppingCart"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="cart" value="${sessionScope.shoppingCart.items}" />
<c:set var="totalPrice" value="${requestScope.totalPrice}" />
<c:set var="user" value="${sessionScope.user}" />

<div class="checkout-page">
    <c:if test="${not empty param.ok && param.ok == true}">
        <div class="order-success">
            <h5 class="text-success">Successfully sent your order!</h5>
            <p>We will contact you soon at <span class="font-weight-bold">${user.email}</span> email address.</p>

            <a href="product" class="btn btn-warning font-weight-bold">Back to shopping!</a>
        </div>
    </c:if>
    <c:if test="${fn:length(cart)>= 1 && param.ok !=true}">
        <div class="container bg-light mt-5 mb-5">
            <div class="py-5 text-center">
                <h2>Checkout</h2>
                <c:if test="${empty param.ok || param.ok == false}">
                    <c:if test="${param.ok == false}">
                        <h5 class="text-danger">Something went wrong... you should check the fields below.</h5>
                    </c:if>

                </div>
                <div class="row">
                    <div class="col-md-4 order-md-2 mb-4">
                        <h4 class="d-flex justify-content-between align-items-center mb-3">
                            <span class="text-muted">Your cart</span>
                            <span class="badge badge-secondary badge-pill"><c:out value="${shoppingCart.numberOfItems}"/></span>
                        </h4> 
                        <ul class="list-group mb-3">
                            <c:forEach var="product" items="${cart}">
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0"><c:out value="${product.item.name}"/></h6>
                                        <small class="text-muted">Quantity: <c:out value="${product.quantity}"/></small>
                                    </div>
                                    <span class="text-muted"><c:out value="${product.item.price}"/></span>
                                </li>
                            </c:forEach>
                            <li class="list-group-item d-flex justify-content-between">
                                <span>Total (USD)</span>
                                <strong><c:out value="${totalPrice}"/></strong>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8 order-md-1">
                        <h4 class="mb-3">Billing details</h4>
                        <form class="needs-validation was-validated" action="cashier" method="POST" enctype="application/x-www-form-urlencoded">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="firstName">First name</label>
                                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="" required="" 
                                           value="<c:out value="${user != null? user.firstName : ''}"/>">
                                    <div class="invalid-feedback">
                                        Valid first name is required.
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="lastName">Last name</label>
                                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="" required=""
                                           value="<c:out value="${user != null? user.lastName : ''}"/>">
                                    <div class="invalid-feedback">
                                        Valid last name is required.
                                    </div>
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="email">Email <span class="text-muted">(Optional)</span></label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="you@example.com" 
                                       value="<c:out value="${user != null? user.email : ''}"/>">
                                <div class="invalid-feedback">
                                    Please enter a valid email address for shipping updates.
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="1234 Main St" required="">
                                <div class="invalid-feedback">
                                    Please enter your shipping address.
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5 mb-3">
                                    <label for="country">Country</label>
                                    <input type="text" class="form-control" id="country" name="country" placeholder="Country" required="">
                                    <div class="invalid-feedback">
                                        Please select a valid country.
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label for="state">State</label>
                                    <input type="text" class="form-control" id="state" name="state" placeholder="State" required=""> 
                                    <div class="invalid-feedback">
                                        Please provide a valid state.
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="zipCode">Zip</label>
                                    <input type="text" class="form-control" name="zipCode" id="zipCode" placeholder="" required="">
                                    <div class="invalid-feedback">
                                        Zip code required.
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-success btn-lg btn-block mb-2" type="submit">Send Order</button>
                        </form>
                    </div>
                </div>
            </c:if>
        </c:if>
        <c:if test="${fn:length(cart)< 1 && param.ok != true}">
            <div class="order-success">
                <h5 class="text-warning">Your shopping cart is empty!</h5>
                <a href="product" class="btn btn-warning font-weight-bold">Back to shopping!</a>
            </div>
        </c:if>
    </div>