<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link href="public/css/homepage.css" rel="stylesheet" type="text/css"/>
<c:import url="/productsOnSale" />

<c:set var="products" value="${requestScope['products']}" />
<div class="home-page ">
    <div class="main-header">
        <div class="">
            <h1 class="display-3">Welcome
                <span>
                    <c:if test="${sessionScope.user != null}">
                        <c:out value="${sessionScope.user.firstName}" />
                    </c:if>
                </span>
                
            </h1>
            <c:if test="${not empty param.msg}">
                <p class="text-success"><c:out value="${param.msg}" /></p>
            </c:if>
            <a href="#itemsOnSale">
                <p>Checkout the latest items!</p>
                <div class="arrow">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </a>
        </div>
    </div>
    <div class="home-products">
        <h1 id="itemsOnSale">ON SALE</h1>
        <div  class="row">
            <c:if test="${fn:length(products)< 1}">
                <div class="alert alert-danger" role="alert">
                    There is nothing in our database with the specified parameters.
                </div>
            </c:if>
            <c:if test="${fn:length(products)>= 1}">  
                <c:forEach var="product" items="${products}">

                    <div class="card col-md-2">
                        <div class="card-image">
                            <img class="card-img-top" src= "${product.imgpath}" alt="Card image cap" /> 
                            <span class="discount-icon"><p>%</p></span>                          
                        </div>

                        <div class="card-body">
                            <h5 class="card-title">
                                <c:out value="${product.name}" />
                            </h5>
                            <h6 class="card-title">
                                <c:out value="${product.manufacturer}" />
                            </h6>
                            <p class="card-text">Some quick example text to build on the card title and make up
                                the bulk of the card's content.</p>
                            <button class="btn btn-primary" onclick="showDetails(${product.productId})" data-toggle="modal" data-target="#myModal">Details</button>
                            <button class="btn btn-warning" onclick="addToCart(${product.productId})">Add</button>
                        </div>
                    </div>
                </c:forEach>
            </c:if>

        </div>
        <hr>
    </div>
</div>