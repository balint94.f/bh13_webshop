<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="contact-page">
     <div class="message-container">
        <c:if test="${not empty param.ok}">

                <c:if test="${param.ok == true}">
                    <p class="text-success h1">Thank you, we'll contact you soon.</p>
                </c:if>
                <c:if test="${param.ok == false}">
                    <p class="text-danger h1">Something went wrong. Try again...</p>
                </c:if>
        </c:if>
    </div>
        <!--Section: Contact v.2-->
        <section class="mb-4 text-light">

            <!--Section heading-->
            <h2 class="h1-responsive font-weight-bold text-center my-4 text-light"></h2>
            <!--Section description-->
            <p class="text-center w-responsive mx-auto mb-5 ">Do you have any questions? Please do not hesitate to contact us directly. Our team will come back to you within
                a matter of hours to help you.</p>

            <div class="row">

                <!--Grid column-->
                <div class="col-md-6 mb-md-0 mb-5">
                    <form id="contact-form" name="contact-form" action="contact" method="POST">

                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <input type="text" id="name" name="name" class="form-control" required="">
                                    <label for="name" class="">Your name</label>
                                </div>
                            </div>
                            <!--Grid column-->

                            <!--Grid column-->
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <input type="text" id="email" name="email" class="form-control" required="">
                                    <label for="email" class="">Your email</label>
                                </div>
                            </div>
                            <!--Grid column-->

                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form mb-0">
                                    <input type="text" id="subject" name="subject" class="form-control" required="">
                                    <label for="subject" class="">Subject</label>
                                </div>
                            </div>
                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row">

                            <!--Grid column-->
                            <div class="col-md-12">

                                <div class="md-form">
                                    <textarea id="message" name="message" rows="2" class="form-control md-textarea" required></textarea>
                                    <label for="message">Your message</label>
                                </div>

                            </div>
                        </div>
                        <!--Grid row-->

                    </form>

                    <div class="text-center text-md-left">
                        <a class="btn btn-primary" onclick="document.getElementById('contact-form').submit();">Send</a>
                    </div>
                    <div class="status"></div>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-3 text-center">
                    <ul class="list-unstyled mb-0">
                        <li><i class="fas fa-map-marker-alt fa-2x"></i>
                            <p> 1121 Budapest, Abig�l utca 1., Hungary</p>
                        </li>

                        <li><i class="fas fa-phone mt-4 fa-2x"></i>
                            <p>+ 36 1 555 555</p>
                        </li>

                        <li><i class="fas fa-at mt-4 fa-2x"></i>
                            <p>info@airsoftstore.com</p>
                        </li>
                    </ul>
                </div>
                <!--Grid column-->

            </div>

        </section>
        <!--Section: Contact v.2-->
</div>