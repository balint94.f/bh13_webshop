window.onload = () => {
    document.getElementById("webpage").style.opacity = 1;
};

window.addEventListener('onbeforeunload', (e) => {
    document.getElementById("webpage").style.opacity = 0;
}, false);

function scrollToAction(id) {
    let container = document.body,
            element = document.getElementById(id);
    container.scrollTop = element.offsetTop;
}

/*
 * FLOAT LOGIN FORM
 *   
 *   */

function showLoginForm() {
    let floatLogin = document.getElementById("float-login");
    floatLogin.style.transform = " translateX(0)";
    floatLogin.style.visibility = 1;
}
document.getElementById("close-login-form").addEventListener("click", function(e){
    e.preventDefault();
    hideLoginForm();
});
function hideLoginForm() {
    let floatLogin = document.getElementById("float-login");
    floatLogin.style.transform = " translateX(-400%)";
    floatLogin.style.visibility = 0;
    document.getElementById("login-msg").innerHTML = "";
}