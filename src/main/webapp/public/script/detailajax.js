async function fetchDetails(productId) {
    let data = JSON.stringify({"id": productId});
    const response = await fetch("productdetail", {
        credentials: "same-origin",
        mode: "same-origin",
        headers: {
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: data
    });
    return response.json();
}


function showDetails (productId){
    currentId=productId;
    console.log(productId);
    fetchDetails(productId)
    .then(res => {
                document.getElementById("modal-product-image").src=res.imgpath;
                document.getElementById("modal-product-name").textContent=res.name;
                document.getElementById("modal-product-manufacturer").textContent=res.manufacturer;
                document.getElementById("modal-product-price").textContent=res.price+" $";
                document.getElementById("modal-product-details").textContent=res.details;
            });
}
