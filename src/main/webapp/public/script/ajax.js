let currentId;
function addToCart(id) {
    sendCartPost(id, "add")
            .then(res => {
                document.getElementById("cart-size").innerHTML = res.cartSize;
            });
}

function addToCartFromModal() {
    sendCartPost(currentId, "add")
            .then(res => {
                document.getElementById("cart-size").innerHTML = res.cartSize;
            });
}

function removeFromCart(id) {
    sendCartPost(id, "remove")
            .then(res => {
                document.getElementById("cart-size").innerHTML = res.cartSize;
                window.location.href = window.location.href;
            });
}

async function sendCartPost(productId, action) {
    let data = JSON.stringify({"id": productId, "action": action});
    const response = await fetch("shoppingCart", {
        credentials: "same-origin",
        mode: "same-origin",
        headers: {
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: data
    });
    return response.json();
}

const loginBtn = document.getElementById("send-login-btn");
const loginForm = document.getElementById("login_form");
loginBtn.addEventListener("click", function (e) {
    e.preventDefault();

    let fields = [];

    const childrenForm = loginForm.childNodes;
    childrenForm.forEach(ch => {
        let field = {}
        if (ch.type == "email") {
            field.name = ch.name;
            field.value = ch.value;
            fields.push(field);
        }
        if (ch.type == "password") {
            field.name = ch.name;
            field.value = ch.value;
            fields.push(field);
        }
    });
    const msgTag = document.getElementById("login-msg");
    sendLoginPost(fields)
            .then(res => {
                if (res.status) {
                    window.location.href = window.location.href;
                } else {
                    msgTag.innerHTML = "Something went wrong, check the fields!";
                    msgTag.classList = '';
                    msgTag.classList.add("text-danger");
                }
            });

});
async function sendLoginPost(fields) {
    let data = JSON.stringify({email: fields[0].value, password: fields[1].value, currentUrl: window.location.href});
    const response = await fetch("login", {
        headers: {
            'Content-Type': 'application/json'
        },
        method: "POST",
        body: data
    });
    return response.json();
}


