
package hu.bh.bh13.webshop.enums;


public enum Input {
    MIN_LENGTH_PASSWORD(6),
    MIN_LENGTH_STRING(3),
    MAX_LENGTH_STRING(200);
    
    private final int size;

    private Input(int size) {
        this.size = size;
    }
    public int size(){
        return this.size;
    }
}
