
package hu.bh.bh13.webshop.enums;

import java.util.regex.Pattern;

/**
 * 
 * @author balat
 * Specifies all forbidden character which cannot be 
 * in any String provided by the client.
 */
public enum BlackList {
    FORBIDDEN_CHARACTERS("^(?!.*:\\||.*<>|.*\\(\\)|.*CR|.*LF)[^&;$%\\'\\\"+\\\\]*$")
    ;
    
    Pattern regExPattern;
    String stringPattern;
    
    private BlackList(String regExPattern) {
        this.stringPattern = regExPattern;
        this.regExPattern = Pattern.compile(regExPattern);
    }
    /**
     * 
     * @return 
     * Returns the FORBIDDEN_CHARACTERS field's pattern.
     */
    public Pattern getRegExPattern() {
        return regExPattern;
    }

    public void setRegExPattern(Pattern regExPattern) {
        this.regExPattern = regExPattern;
    }
     /**
     * 
     * @return 
     * Returns the FORBIDDEN_CHARACTERS field's pattern as String.
     */
    public String getStringPattern() {
        return stringPattern;
    }

    public void setStringPattern(String stringPattern) {
        this.stringPattern = stringPattern;
    }
    
    
}
