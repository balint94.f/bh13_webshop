
package hu.bh.bh13.webshop.enums;


public enum WebPages {
    MAIN_PAGE("index.jsp"),
    HOME("home"),
    REGISTRATION("registration"),
    LOGIN("login"),
    PRODUCT("product"),
    PRODUCT_DETAIL("productDetail"),
    ABOUT_US("aboutus"),
    SHOPPING_CART("shoppingCart"),
    FAQ("faq"),
    CHECKOUT("checkout"),
    CHANGE_PWD("changepw"),
    CONTACT("contact"),
    FORGOTTEN_PWD("forgottenpw");
    
    String page;
    private WebPages(String page){
        this.page = page;
    }

    public String getPage() {
        return page;
    }
}
