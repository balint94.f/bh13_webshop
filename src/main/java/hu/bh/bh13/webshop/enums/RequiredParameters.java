
package hu.bh.bh13.webshop.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * 
 * Contains the required parameters has to sent by the client.
 */
public enum RequiredParameters {
    REGISTRATION(new String[]{"lastName", "firstName", "email", "pwd1", "pwd2"}),
    LOGIN(new String[]{"email", "password"}),
    CHECKOUT(new String[]{"firstName", "lastName", "email", "address", "country", "state", "zipCode"}),
    CHANGE_PWD(new String[]{"oldPassword", "newPassword", "confirmNewPassword"})
    ;

    private List<String> parameters = new ArrayList<>();
    
    private RequiredParameters(String[] params) {
        this.parameters = Arrays.asList(params);
    }
    
    public List<String> getParameters() {
        return this.parameters;
    }
    
}
