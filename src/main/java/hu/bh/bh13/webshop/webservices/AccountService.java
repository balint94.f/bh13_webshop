package hu.bh.bh13.webshop.webservices;

import hu.bh.bh13.webshop.daos.AccountDAO;
import hu.bh.bh13.webshop.dtos.AccountDTO;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("accounts")
@Stateless
public class AccountService {

    @Inject
    AccountDAO accDao;

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response showAllAccounts() {
        return Response.ok(Arrays.asList(accDao.getAllAccountDTO())).build();
    }

    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response getAccountById(@PathParam("id") long id) {
        try {
            return Response.ok(Arrays.asList(accDao.findById(id))).build();
        } catch (Exception ex) {
            Logger.getLogger(AccountService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.NOT_FOUND).build();
        }

    }

    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    @Consumes(value = MediaType.APPLICATION_JSON)
    public Response create(AccountDTO account) {
        System.out.println(account);
        try {
            accDao.insert(account);
        } catch (Exception ex) {
            Logger.getLogger(HealthCheckService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok(Arrays.asList(account)).build();
    }

    @DELETE
    @Path("{id}")
    public String delete(@PathParam("id") long id) {
        System.out.println("delete is running");
        if (accDao.deleteAccount(id)) {
            return "Account has been deleted! account id: " + id;
        }
        return "Sikertelen torles";
    }

    @PUT
    @Produces(value = MediaType.TEXT_PLAIN)
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Path("{id}")
    public String put(@PathParam("id") long id, AccountDTO account) {
        System.out.println("PUT is running");
        try {
            account.setId(id);
            accDao.updateAccount(account);
            return "Sikeres update";
        } catch (Exception ex) {
            Logger.getLogger(HealthCheckService.class.getName()).log(Level.SEVERE, null, ex);
            return "Sikertelen update";
        }

    }
}
