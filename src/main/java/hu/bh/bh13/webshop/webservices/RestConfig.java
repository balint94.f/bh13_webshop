
package hu.bh.bh13.webshop.webservices;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(value = "api")
public class RestConfig extends Application {
    
} 

