package hu.bh.bh13.webshop.webservices;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("health_check")
@Stateless
public class HealthCheckService {
    
    
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response healthCheck() {
        return Response.ok("ok").build();
    }
    
}
