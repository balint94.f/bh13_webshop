package hu.bh.bh13.webshop.json;

public class CartRequest {
    private long id;
    private String action;
    
    public long getId() {return id;}

    public String getAction() {return action;}

    @Override
    public String toString() {
        return "CartRequest{" + "id=" + id + ", action=" + action + '}';
    }
}
