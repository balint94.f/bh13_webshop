package hu.bh.bh13.webshop.json;

public class CartResponse {
    private int cartSize;
    private String message;
    private Boolean status;
    
    public int getCartSize() {
        return cartSize;
    }

    public void setCartSize(int cartSize) {
        this.cartSize = cartSize;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CartResponse{" + "cartSize=" + cartSize + ", message=" + message + '}';
    }
}
