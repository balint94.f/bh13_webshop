
package hu.bh.bh13.webshop.json;
public class DetailRequest {
    
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
}
