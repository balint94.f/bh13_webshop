package hu.bh.bh13.webshop.dtos;

import hu.bh.bh13.webshop.entities.Account;
import hu.bh.bh13.webshop.entities.Product;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class OrdersDTO {

    private long id;
    private Account account;
    private Date dateOfOrder;
    private List<Product> productList;

    public OrdersDTO() {
    }

    public OrdersDTO(long id, Account account, Date dateOfOrder, List<Product> productList) {
        this.id = id;
        this.account = account;
        this.dateOfOrder = dateOfOrder;
        this.productList = productList;
    }

    public OrdersDTO(Account account, Date dateOfOrder, List<Product> productList) {
        this.account = account;
        this.dateOfOrder = dateOfOrder;
        this.productList = productList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Date getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(Date dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "OrdersDTO{" + "id=" + id + ", account=" + account + ", dateOfOrder=" + dateOfOrder + ", productList=" + productList + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 71 * hash + Objects.hashCode(this.account);
        hash = 71 * hash + Objects.hashCode(this.dateOfOrder);
        hash = 71 * hash + Objects.hashCode(this.productList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdersDTO other = (OrdersDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.account, other.account)) {
            return false;
        }
        if (!Objects.equals(this.dateOfOrder, other.dateOfOrder)) {
            return false;
        }
        if (!Objects.equals(this.productList, other.productList)) {
            return false;
        }
        return true;
    }

    public static class Builder {

        private long id;
        private Account account;
        private Date dateOfOrder;
        private List<Product> productList;

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setAccount(Account account) {
            this.account = account;
            return this;
        }

        public Builder setDateOfOrder(Date dateOfOrder) {
            this.dateOfOrder = dateOfOrder;
            return this;
        }

        public Builder setProductList(List<Product> productList) {
            this.productList = productList;
            return this;
        }

        public OrdersDTO build() {
            return new OrdersDTO(account, dateOfOrder, productList);
        }

        public OrdersDTO buildWithId() {
            return new OrdersDTO(id, account, dateOfOrder, productList);
        }
    }
}
