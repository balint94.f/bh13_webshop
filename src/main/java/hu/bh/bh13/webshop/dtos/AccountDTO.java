
package hu.bh.bh13.webshop.dtos;

import hu.bh.bh13.webshop.entities.OrderE;
import java.util.List;

public class AccountDTO {
    private long id;
    private String firstName, lastName, email, password;
    private int accountRole = 1;
    private List<OrderDTO> orders; 
    private byte[] salt;

    public AccountDTO() {
    }

    public AccountDTO(long id, String firstName, String lastName, String email, String password, int accountRole, List<OrderDTO> orders, byte[] salt) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.accountRole = accountRole;
        this.orders = orders;
        this.salt = salt;
    }

    public AccountDTO(String firstName, String lastName, String email, String password, byte[] salt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.salt = salt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAccountRole() {
        return accountRole;
    }

    public void setAccountRole(int accountRole) {
        this.accountRole = accountRole;
    }

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }
    

    @Override
    public String toString() {
        return "AccountDTO{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", password=" + password + ", accountRole=" + accountRole + ", orders=" + orders + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AccountDTO other = (AccountDTO) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    public static class Builder {
        private long id;
        private String firstName, lastName, email, password;
        private int accountRole;
        private List<OrderDTO> orders; 
        private byte[] salt;

        public Builder setId(long id) {
            this.id = id;
            return this;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder setAccountRole(int accountRole) {
            this.accountRole = accountRole;
            return this;
        }

        public Builder setOrders(List<OrderDTO> orders) {
            this.orders = orders;
            return this;
        }

        public Builder setSalt(byte[] salt) {
            this.salt = salt;
            return this;
        }
        
        public AccountDTO build(){
            return new AccountDTO(firstName, lastName, email, password, salt);
        }
        
        public AccountDTO buildFull(){
            return new AccountDTO(id, firstName, lastName, email, password, accountRole, orders, salt);
        }
        
    }
    
}
