package hu.bh.bh13.webshop.dtos;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class OrderDTO {

    private static final long serialVersionUID = 1L;
    private Long orderId;
    private AccountDTO account;
    private Date dateOfOrder;
    private List<ProductDTO> productList;
    private String address;
    private String country;
    private String state;
    private int zipCode;

    public OrderDTO() {
    }

    public OrderDTO(Long orderId, AccountDTO account, Date dateOfOrder, List<ProductDTO> productList, String address, String country, String state, int zipCode) {
        this.orderId = orderId;
        this.account = account;
        this.dateOfOrder = dateOfOrder;
        this.productList = productList;
        this.address = address;
        this.country = country;
        this.state = state;
        this.zipCode = zipCode;
    }

    public OrderDTO(AccountDTO account, Date dateOfOrder, List<ProductDTO> productList, String address, String country, String state, int zipCode) {
        this.account = account;
        this.dateOfOrder = dateOfOrder;
        this.productList = productList;
        this.address = address;
        this.country = country;
        this.state = state;
        this.zipCode = zipCode;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    public Date getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(Date dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public List<ProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDTO> productList) {
        this.productList = productList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.orderId);
        hash = 97 * hash + Objects.hashCode(this.account);
        hash = 97 * hash + Objects.hashCode(this.dateOfOrder);
        hash = 97 * hash + Objects.hashCode(this.productList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderDTO other = (OrderDTO) obj;
        if (!Objects.equals(this.orderId, other.orderId)) {
            return false;
        }
        if (!Objects.equals(this.account, other.account)) {
            return false;
        }
        if (!Objects.equals(this.dateOfOrder, other.dateOfOrder)) {
            return false;
        }
        if (!Objects.equals(this.productList, other.productList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OrderDTO{" + "orderId=" + orderId + ", account=" + account + ", dateOfOrder=" + dateOfOrder + ", productList=" + productList + ", address=" + address + ", country=" + country + ", state=" + state + ", zipCode=" + zipCode + '}';
    }

   


    public static class Builder {

        private Long orderId;
        private AccountDTO account;
        private Date dateOfOrder;
        private List<ProductDTO> productList;
        private String details;
        private String address;
        private String country;
        private String state;
        private int zipCode;
        

       
        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setCountry(String country) {
            this.country = country;
            return this;
        }

        public Builder setState(String state) {
            this.state = state;
            return this;
        }

        public Builder setZipCode(int zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public Builder setOrderId(Long orderId) {
            this.orderId = orderId;
            return this;
        }

        public Builder setAccount(AccountDTO account) {
            this.account = account;
            return this;
        }

        public Builder setDateOfOrder(Date dateOfOrder) {
            this.dateOfOrder = dateOfOrder;
            return this;
        }

        public Builder setProductList(List<ProductDTO> productList) {
            this.productList = productList;
            return this;
        }

        public Builder setDetails(String details) {
            this.details = details;
            return this;
        }

        public OrderDTO build() {
            return new OrderDTO(orderId, account, dateOfOrder, productList, address, country, state, zipCode);
        }
    }
}
