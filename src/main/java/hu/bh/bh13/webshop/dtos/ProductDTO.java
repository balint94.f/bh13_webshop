package hu.bh.bh13.webshop.dtos;

import hu.bh.bh13.webshop.entities.OrderE;
import hu.bh.bh13.webshop.entities.Productimage;

public class ProductDTO {

    private Long productId;
    private String name;
    private Double price;
    private String manufacturer;
    private Boolean onSale;
    private OrderE orderId;
    private String details;
    private String imgpath;
    private Productimage productimage;

    public ProductDTO() {
    }

    public ProductDTO(Long productId, String name, Double price, String manufacturer, Boolean onSale, OrderE orderId, String details, String imgpath) {
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.manufacturer = manufacturer;
        this.onSale = onSale;
        this.orderId = orderId;
        this.details = details;
        this.imgpath = imgpath;
    }

    public ProductDTO(String name, Double price, String manufacturer, Boolean onSale, OrderE orderId, String details, String imgpath) {
        this.name = name;
        this.price = price;
        this.manufacturer = manufacturer;
        this.onSale = onSale;
        this.orderId = orderId;
        this.details = details;
        this.imgpath = imgpath;
    }

    public ProductDTO(Long productId, String name, Double price, String manufacturer, Boolean onSale, OrderE orderId, String details, String imgpath, Productimage productimage) {
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.manufacturer = manufacturer;
        this.onSale = onSale;
        this.orderId = orderId;
        this.details = details;
        this.imgpath = imgpath;
        this.productimage = productimage;
    }

    public Productimage getProductimage() {
        return productimage;
    }

    public void setProductimage(Productimage productimage) {
        this.productimage = productimage;
    }

    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Boolean getOnSale() {
        return onSale;
    }

    public void setOnSale(Boolean onSale) {
        this.onSale = onSale;
    }

    public OrderE getOrderId() {
        return orderId;
    }

    public void setOrderId(OrderE orderId) {
        this.orderId = orderId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.productId != null ? this.productId.hashCode() : 0);
        hash = 37 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 37 * hash + (this.price != null ? this.price.hashCode() : 0);
        hash = 37 * hash + (this.manufacturer != null ? this.manufacturer.hashCode() : 0);
        hash = 37 * hash + (this.onSale != null ? this.onSale.hashCode() : 0);
        hash = 37 * hash + (this.orderId != null ? this.orderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductDTO other = (ProductDTO) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.manufacturer == null) ? (other.manufacturer != null) : !this.manufacturer.equals(other.manufacturer)) {
            return false;
        }
        if (this.productId != other.productId && (this.productId == null || !this.productId.equals(other.productId))) {
            return false;
        }
        if (this.price != other.price && (this.price == null || !this.price.equals(other.price))) {
            return false;
        }
        if (this.onSale != other.onSale && (this.onSale == null || !this.onSale.equals(other.onSale))) {
            return false;
        }
        if (this.orderId != other.orderId && (this.orderId == null || !this.orderId.equals(other.orderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProductDTO{" + "productId=" + productId + ", name=" + name + ", price=" + price + ", manufacturer=" + manufacturer + ", onSale=" + onSale + ", orderId=" + orderId + ", details=" + details + ", imgpath=" + imgpath + '}';
    }

    public static class Builder {

        private Long productId;
        private String name;
        private Double price;
        private String manufacturer;
        private Boolean onSale;
        private OrderE orderId;
        private String details;
        private String imgpath;
        private Productimage productimage;

        public Builder setImgpath(String imgpath) {
            this.imgpath = imgpath;
            return this;
        }

        public Builder setProductimage(Productimage image) {
            this.productimage = image;
            return this;
        }

        public Builder setProductId(Long productId) {
            this.productId = productId;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setPrice(Double price) {
            this.price = price;
            return this;
        }

        public Builder setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }

        public Builder setOnSale(Boolean onSale) {
            this.onSale = onSale;
            return this;
        }

        public Builder setOrderId(OrderE orderId) {
            this.orderId = orderId;
            return this;
        }

        public Builder setDetails(String details) {
            this.details = details;
            return this;
        }

        public ProductDTO build() {
            return new ProductDTO(name, price, manufacturer, onSale, orderId, details, imgpath);
        }

        public ProductDTO buildwithId() {
            return new ProductDTO(productId, name, price, manufacturer, onSale, orderId, details, imgpath, productimage);
        }
    }
}
