/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.bh13.webshop.servlets;

import hu.bh.bh13.webshop.enums.WebPages;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author balat
 */
public class ContactServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("contentPage", WebPages.CONTACT.getPage());
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // WIP returns success on ever call
        request.getSession().setAttribute("contentPage", WebPages.CONTACT.getPage());
        Boolean success = true;
        response.sendRedirect(request.getContextPath() + "/contact?ok=" + success);
    }
    

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
