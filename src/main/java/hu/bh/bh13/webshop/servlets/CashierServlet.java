/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.bh13.webshop.servlets;

import hu.bh.bh13.webshop.cart.ShoppingCart;
import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.dtos.OrderDTO;
import hu.bh.bh13.webshop.enums.RequiredParameters;
import hu.bh.bh13.webshop.enums.WebPages;
import hu.bh.bh13.webshop.services.CashierService;
import hu.bh.bh13.webshop.services.InputValidation;
import hu.bh.bh13.webshop.services.ShoppingCartHandler;
import java.io.IOException;
import java.util.Date;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CashierServlet extends HttpServlet {

    @Inject
    ShoppingCartHandler cartHandler;
    @Inject
    InputValidation validation;
    @Inject
    CashierService cashierService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        double totalPrice = cartHandler.totalPrice((ShoppingCart) session.getAttribute("shoppingCart"));
        request.setAttribute("totalPrice", totalPrice);
        session.setAttribute("contentPage", WebPages.CHECKOUT.getPage());
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contentPage = WebPages.CHECKOUT.getPage();
        Boolean success = false;
        HttpSession session = request.getSession(false);

        try {
            /*
            If the sent parameters are valid and the user is logged in, 
            it is going to save the specified order. Otherwise throw exception.
             */
            ShoppingCart cart = (ShoppingCart) session.getAttribute("shoppingCart");

            if (session.getAttribute("user") != null
                    && cart != null && validation.validate(request.getParameterMap(), RequiredParameters.CHECKOUT.getParameters())) {

                OrderDTO order = new OrderDTO.Builder()
                        .setCountry((String) request.getParameter("country"))
                        .setAddress((String) request.getParameter("address"))
                        .setState((String) request.getParameter("state"))
                        .setZipCode(Integer.parseInt(request.getParameter("zipCode")))
                        .setAccount((AccountDTO) session.getAttribute("user"))
                        .setDateOfOrder(new Date())
                        .build();
                cashierService.saveOrder(order, cart);
                success = true;
                //message = "You've successfully sent your order";
            } else {
                success = false;
                // message = "Invalid input. Check the input fields!";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        session.setAttribute("contentPage", contentPage);
        response.sendRedirect(request.getContextPath() + "/cashier?ok=" + success);
    }

    @Override
    public String getServletInfo() {
        return "cashier servlet";
    }

}
