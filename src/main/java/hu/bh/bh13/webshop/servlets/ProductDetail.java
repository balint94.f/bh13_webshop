package hu.bh.bh13.webshop.servlets;

import com.google.gson.Gson;
import hu.bh.bh13.webshop.daos.ProductsDAO;
import hu.bh.bh13.webshop.dtos.ProductDTO;
import hu.bh.bh13.webshop.json.DetailRequest;
import hu.bh.bh13.webshop.json.DetailResponse;
import hu.bh.bh13.webshop.services.InputValidation;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProductDetail extends HttpServlet {

    @Inject
    ProductsDAO pdao;
    @Inject
    InputValidation validation;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String requestData = request.getReader().lines().collect(Collectors.joining());
        Gson gson = new Gson();
        DetailRequest detailReq;
        DetailResponse detailResp = new DetailResponse();
        ProductDTO product;
        try {
            detailReq = gson.fromJson(requestData, DetailRequest.class);
            if (validation.validateFields(detailReq)) {
                product = pdao.findById(detailReq.getId());
                if (product != null) {
                    detailResp.setDetails(product.getDetails());
                    detailResp.setImgpath(product.getImgpath());
                    detailResp.setManufacturer(product.getManufacturer());
                    detailResp.setName(product.getName());
                    detailResp.setPrice(product.getPrice());
                    detailResp.setProductId(product.getProductId());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ProductDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        String jsonResponse = gson.toJson(detailResp);
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(jsonResponse);
        out.flush();

    }
}
