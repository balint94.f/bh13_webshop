package hu.bh.bh13.webshop.servlets;

import hu.bh.bh13.webshop.daos.ProductsDAO;
import hu.bh.bh13.webshop.dtos.ProductDTO;
import hu.bh.bh13.webshop.enums.WebPages;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Request;

public class ProductServlet extends HttpServlet {

    @Inject
    ProductsDAO pdao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String message = "";
        List<ProductDTO> products = new ArrayList<>();
        if (request.getParameter("search") != null && request.getParameter("searchType") != null) {
            String search = request.getParameter("search");
            String searchType = request.getParameter("searchType");
            
            switch (searchType) {
                case "manufacturer": {
                    try {
                        products.addAll(pdao.findByManufacturer(search));
                    } catch (Exception ex) {
                        message = "Unable to find any item with the specified manufacturer.";
                        ex.printStackTrace();
                    }
                    break;
                }
                case "name": {
                    try {
                        products.addAll(pdao.findByNameContains(search));
                    } catch (Exception ex) {
                        message = "Unable to find any item with the specified name.";
                        ex.printStackTrace();
                    }
                    break;
                }
            }
        } else {
            products.addAll(pdao.getAllProducts());
        }
        request.setAttribute("products", products);
        request.setAttribute("message", message);
        request.getSession().setAttribute("contentPage", WebPages.PRODUCT.getPage());
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ProductDTO dto = new ProductDTO.Builder().setName(request.getParameter("name"))
                .setManufacturer(request.getParameter("manufacturer"))
                .setOnSale(Boolean.parseBoolean(request.getParameter("sale")))
                .setPrice(Double.parseDouble(request.getParameter("price")))
                .setDetails(request.getParameter("details"))
                .build();

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
