
package hu.bh.bh13.webshop.servlets;

import hu.bh.bh13.webshop.enums.WebPages;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Index extends HttpServlet {

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("contentPage", WebPages.HOME.getPage());
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);
    }
    @Override
    public String getServletInfo() {
        return "Set home page";
    }

}
