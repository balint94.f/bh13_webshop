package hu.bh.bh13.webshop.servlets;

import com.google.gson.Gson;
import hu.bh.bh13.webshop.cart.ShoppingCart;
import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.enums.Input;
import hu.bh.bh13.webshop.enums.RequiredParameters;
import hu.bh.bh13.webshop.enums.WebPages;
import hu.bh.bh13.webshop.json.LoginRequest;
import hu.bh.bh13.webshop.json.LoginResponse;
import hu.bh.bh13.webshop.services.LoginCustomer;
import hu.bh.bh13.webshop.services.InputValidation;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    @Inject
    LoginCustomer loginCustomer;
    @Inject
    InputValidation validation;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("contentPage", WebPages.HOME.getPage());
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // Read JSON from HTTP POST 
        
        Gson gson = new Gson();
        LoginRequest loginReq;
        LoginResponse loginResp = new LoginResponse();
        loginResp.setStatus(false);
        try {
            String requestData = request.getReader().lines().collect(Collectors.joining());
            loginReq = gson.fromJson(requestData, LoginRequest.class);
            
            // Validate the request parameters
            if (validation.validateFields(loginReq) 
                    && loginReq.getPassword().length() >= Input.MIN_LENGTH_PASSWORD.size()) {
                
                AccountDTO account = new AccountDTO.Builder()
                        .setEmail(loginReq.getEmail())
                        .setPassword(loginReq.getPassword())
                        .build();

                AccountDTO existingAccount = loginCustomer.login(account);
                // Able to login
                if (existingAccount != null) {
                    HttpSession session = request.getSession();
                    session.setAttribute("user", existingAccount);
                    
                    // bound user to the current cart
                    ShoppingCart cart = (ShoppingCart) session.getAttribute("shoppingCart");
                    if (cart != null) {
                        cart.setAccount(existingAccount);
                    }
                    loginResp.setStatus(true);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // Write JSON respons
        String jsonResponse = gson.toJson(loginResp);
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(jsonResponse);
        out.flush();
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
