package hu.bh.bh13.webshop.servlets;

import hu.bh.bh13.webshop.daos.ProductsDAO;
import hu.bh.bh13.webshop.dtos.ProductDTO;
import hu.bh.bh13.webshop.enums.WebPages;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProductsOnSale extends HttpServlet {

    @Inject
    ProductsDAO pdao;

    public ProductsOnSale() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<ProductDTO> products = new ArrayList<>();

        try {
            products = pdao.findBySale();
        } catch (Exception ex) {
            ex.printStackTrace();
            request.setAttribute("contentPage", "error");
            request.setAttribute("errorMessage", ex.getMessage());
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        
        request.setAttribute("products", products);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    public String getServletInfo() {
        return "Fetch all product on sale";
    }

}
