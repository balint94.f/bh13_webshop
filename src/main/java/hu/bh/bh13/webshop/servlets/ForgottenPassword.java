package hu.bh.bh13.webshop.servlets;

import com.sun.webkit.WebPage;
import hu.bh.bh13.webshop.daos.AccountDAO;
import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.enums.WebPages;
import hu.bh.bh13.webshop.utils.HashTool;
import hu.bh.bh13.webshop.utils.RandomPasswordGenerator;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ForgottenPassword extends HttpServlet {

    @Inject
    RandomPasswordGenerator randomPasswordGenerator;
    @Inject
    HashTool hashTool;
    @Inject
    AccountDAO accountDao;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("contentPage", WebPages.FORGOTTEN_PWD.getPage());
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String message = "", contentPage = WebPages.FORGOTTEN_PWD.getPage();
        try {
            String email = request.getParameter("email");
            AccountDTO account = accountDao.findByEmail(request.getParameter("email"));
            if (email != null && !"".equals(email) && account != null) {
                String randomPassword = randomPasswordGenerator.generateRandomPassword();
                byte[] salt = hashTool.getSalt();
                account.setPassword(hashTool.getSecurePassword(randomPassword, salt));
                account.setSalt(salt);
                accountDao.updateAccount(account);
                message = "Successfully updated the password.";
                System.out.println("New password: " + randomPassword);
                contentPage = WebPages.HOME.getPage();
            } else {
                message = "Invalid parameters. Check the input fields!";
            }
        } catch (Exception ex) {
            Logger.getLogger(ForgottenPassword.class.getName()).log(Level.SEVERE, null, ex);

        }
        request.getSession().setAttribute("contentPage", contentPage);
        response.sendRedirect(request.getContextPath());
    }

    @Override
    public String getServletInfo() {
        return "Generate new password";
    }

}
