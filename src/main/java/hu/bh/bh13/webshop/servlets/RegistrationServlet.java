package hu.bh.bh13.webshop.servlets;

import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.enums.RequiredParameters;
import hu.bh.bh13.webshop.enums.WebPages;
import hu.bh.bh13.webshop.services.InputValidation;
import hu.bh.bh13.webshop.services.RegistrateCustomer;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrationServlet extends HttpServlet {

    @Inject
    RegistrateCustomer registrateService;
    @Inject
    InputValidation validation;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getSession().setAttribute("contentPage", WebPages.REGISTRATION.getPage());
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String contentPage = WebPages.REGISTRATION.getPage(), message = "";
        try {
            if (validation.validate(request.getParameterMap(), RequiredParameters.REGISTRATION.getParameters())) {
                String pwd1 = request.getParameter("pwd1"), pwd2 = request.getParameter("pwd2");
                if (pwd1.equals(pwd2)) {
                    AccountDTO account = new AccountDTO.Builder()
                            .setFirstName(request.getParameter("firstName"))
                            .setLastName(request.getParameter("lastName"))
                            .setEmail(request.getParameter("email"))
                            .setPassword(request.getParameter("pwd1"))
                            .build();
                    if (registrateService.registrate(account)) {
                        contentPage = WebPages.HOME.getPage();
                    } else {
                        message = "The given email is occupied.";
                    }
                } else {
                    message = "Both password have to match.";
                }
            } else {
                message = "Invalid parameters. Check the input fields.";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getSession().setAttribute("contentPage", contentPage);
        response.sendRedirect(request.getContextPath() + "/" +contentPage+ "?msg=" + message);
    }

    @Override
    public String getServletInfo() {
        return "Registration servlet";
    }
}
