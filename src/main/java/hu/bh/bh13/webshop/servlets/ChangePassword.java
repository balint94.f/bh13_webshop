package hu.bh.bh13.webshop.servlets;

import hu.bh.bh13.webshop.daos.AccountDAO;
import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.enums.RequiredParameters;
import hu.bh.bh13.webshop.enums.WebPages;
import hu.bh.bh13.webshop.services.InputValidation;
import hu.bh.bh13.webshop.utils.HashTool;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangePassword extends HttpServlet {

    @Inject
    HashTool hashTool;
    @Inject
    AccountDAO accountDao;
    @Inject
    InputValidation validation;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contentPage
                = request.getSession().getAttribute("user") != null
                ? WebPages.CHANGE_PWD.getPage()
                : WebPages.LOGIN.getPage();

        request.getSession().setAttribute("contentPage", contentPage);
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String contentPage = WebPages.CHANGE_PWD.getPage(), message = "";
        HttpSession session = request.getSession();
        try {
            if (session.getAttribute("user") != null 
                    && validation.validate(request.getParameterMap(), RequiredParameters.CHANGE_PWD.getParameters())) {

                AccountDTO account = (AccountDTO) request.getSession().getAttribute("user");

                String oldPassword = request.getParameter("oldPassword");
                String newPassword = request.getParameter("newPassword");
                String confirmNewPassword = request.getParameter("confirmNewPassword");
                String hashedOldPassword = hashTool.getSecurePassword(request.getParameter("oldPassword"), account.getSalt());

                if (oldPassword != null && newPassword != null
                        && confirmNewPassword != null
                        && hashedOldPassword.equals(account.getPassword())
                        && newPassword.equals(confirmNewPassword)) {

                    byte[] salt = hashTool.getSalt();
                    account.setPassword(hashTool.getSecurePassword(newPassword, salt));
                    account.setSalt(salt);

                    accountDao.updateAccount(account);

                    message = "Your password is successfully updated.";
                } else {
                    message = "Unable to save the given password.";
                } 
            } else {
                message = "Invalid input. Check the fields...";
            } 
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        session.setAttribute("contentPage", contentPage);
        response.sendRedirect(request.getContextPath() + "?msg=" + message);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
