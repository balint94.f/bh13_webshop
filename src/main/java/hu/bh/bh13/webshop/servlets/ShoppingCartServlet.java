package hu.bh.bh13.webshop.servlets;

import com.google.gson.Gson;
import hu.bh.bh13.webshop.cart.ShoppingCart;
import hu.bh.bh13.webshop.enums.WebPages;
import hu.bh.bh13.webshop.json.CartRequest;
import hu.bh.bh13.webshop.json.CartResponse;
import hu.bh.bh13.webshop.services.InputValidation;
import hu.bh.bh13.webshop.services.ShoppingCartHandler;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ShoppingCartServlet extends HttpServlet {

    @Inject
    ShoppingCartHandler cartService;
    @Inject
    InputValidation validation;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        session.setAttribute("contentPage", WebPages.SHOPPING_CART.getPage());
        request.getRequestDispatcher(WebPages.MAIN_PAGE.getPage()).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        ShoppingCart cart = (ShoppingCart) session.getAttribute("shoppingCart");

        Gson gson = new Gson();
        CartResponse cartResp = new CartResponse();
        CartRequest cartReq;
        try {
            String requestData = req.getReader().lines().collect(Collectors.joining());
            cartReq = gson.fromJson(requestData, CartRequest.class);
            if (validation.validateFields(cartReq)) {
                switch (cartReq.getAction()) {
                    case "add":
                        cartService.add(cartReq.getId(), cart);
                        cartResp.setStatus(true);
                        cartResp.setMessage("Successfully added");
                        break;
                    case "remove":
                        cartService.remove(cartReq.getId(), cart);
                        cartResp.setStatus(true);
                        cartResp.setMessage("Successfully removed");
                        break;
                    default:
                        cartResp.setStatus(false);
                        cartResp.setMessage("Unable to perform the given action.");
                }
            } else {
                cartResp.setStatus(false);
                cartResp.setMessage("Invalid input.");
            }
        } catch (Exception ex) {
            cartResp.setMessage("Unable to perform the action.");
            Logger.getLogger(ShoppingCartServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        cartResp.setCartSize(cart.getNumberOfItems());
        String jsonResponse = gson.toJson(cartResp);

        PrintWriter out = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        out.print(jsonResponse);
        out.flush();
    }

    @Override
    public String getServletInfo() {
        return "ShoppingCart endpoint";
    }

}
