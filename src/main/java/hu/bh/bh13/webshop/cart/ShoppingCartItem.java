
package hu.bh.bh13.webshop.cart;

import hu.bh.bh13.webshop.dtos.ProductDTO;

public class ShoppingCartItem {
    ProductDTO item;
    int quantity;
    long id;
    
    public ShoppingCartItem(long id, ProductDTO anItem) {
        item = anItem;
        quantity = 1;
        this.id = id;
    }

    public void incrementQuantity() {
        quantity++;
    }

    public void decrementQuantity() {
        quantity--;
    }

    public ProductDTO getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "ShoppingCartItem{" + "item=" + item + ", quantity=" + quantity + ", id=" + id + '}';
    }
    
}

