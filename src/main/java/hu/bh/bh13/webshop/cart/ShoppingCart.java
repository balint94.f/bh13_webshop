
package hu.bh.bh13.webshop.cart;

import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.dtos.ProductDTO;
import hu.bh.bh13.webshop.entities.Account;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ShoppingCart {
    HashMap<Long, ShoppingCartItem> items = null;
    int numberOfItems = 0;
    AccountDTO account;
    
    public ShoppingCart() {
        items = new HashMap<>();
    }

    public synchronized void add(ShoppingCartItem product) {
        if (items.containsKey(product.id)) {
            ShoppingCartItem scitem = (ShoppingCartItem) items.get(product.id);
            scitem.incrementQuantity();
        } else {
            items.put(product.id, product);
        }
    }

    public synchronized void remove(Long productId) {
        if (items.containsKey(productId)) {
            ShoppingCartItem scitem = (ShoppingCartItem) items.get(productId);
            scitem.decrementQuantity();

            if (scitem.getQuantity() <= 0) {
                items.remove(productId);
            }

            numberOfItems--;
        }
    }

    public int size(){
        return items.size();
    }
    
    public synchronized List<ShoppingCartItem> getItems() {
        List<ShoppingCartItem> results = new ArrayList<>();
        results.addAll(this.items.values());

        return results;
    }

    protected void finalize() throws Throwable {
        items.clear();
    }

    public synchronized int getNumberOfItems() {
        numberOfItems = 0;

        for (Iterator i = getItems().iterator(); i.hasNext();) {
            ShoppingCartItem item = (ShoppingCartItem) i.next();
            numberOfItems += item.getQuantity();
        }

        return numberOfItems;
    }

    public synchronized double getTotal() {
        double amount = 0.0;

        for (Iterator i = getItems()
                              .iterator(); i.hasNext();) {
            ShoppingCartItem item = (ShoppingCartItem) i.next();
            ProductDTO product = (ProductDTO) item.getItem();

            amount += (item.getQuantity() * product.getPrice());
        }

        return roundOff(amount);
    }

    private double roundOff(double x) {
        long val = Math.round(x * 100); // cents

        return val / 100.0;
    }

    public synchronized void clear() {
        items.clear();
        numberOfItems = 0;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

}
