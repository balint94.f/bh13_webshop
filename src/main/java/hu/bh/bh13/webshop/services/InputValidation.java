package hu.bh.bh13.webshop.services;

import hu.bh.bh13.webshop.enums.BlackList;
import hu.bh.bh13.webshop.enums.Input;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;

@Stateless
public class InputValidation {

    public boolean validate(Map<String, String[]> params, List<String> requiredParams) {
        boolean safe = false;
        Iterator<Map.Entry<String, String[]>> itr = params.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, String[]> entry = itr.next();
            safe = validateParam(entry.getKey(), requiredParams) && validateValues(entry.getValue());
        }
        return safe;
    }

    public boolean validateParam(String paramter, List<String> requiredParams) {
        System.out.println("KEY: "+paramter ) ;
        return paramter != null
                && paramter.length() >= Input.MIN_LENGTH_STRING.size()
                && requiredParams.contains(paramter);
    }

    public boolean validateValues(String[] values) {
        boolean safe = false;
        for (String value : values) {
            System.out.println("VALIE" + value);
            safe
                    = value != null
                    && value.length() >= Input.MIN_LENGTH_STRING.size()
                    && value.length() <= Input.MAX_LENGTH_STRING.size()
                    && BlackList.FORBIDDEN_CHARACTERS
                            .getRegExPattern()
                            .matcher(value)
                            .find();
        }
        return safe;
    }

    public <T> boolean validateFields(T object) {
        Field[] fields = object.getClass().getDeclaredFields();
        boolean safe = false;
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object value = field.get(object);
                safe = value != null 
                        && BlackList.FORBIDDEN_CHARACTERS
                        .getRegExPattern()
                        .matcher(value.toString())
                        .find();
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(InputValidation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(InputValidation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return safe;
    }
}
