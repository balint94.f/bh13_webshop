package hu.bh.bh13.webshop.services;

import hu.bh.bh13.webshop.cart.ShoppingCart;
import hu.bh.bh13.webshop.daos.OrderDAO;
import hu.bh.bh13.webshop.dtos.OrderDTO;
import hu.bh.bh13.webshop.dtos.ProductDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class CashierService {

    @Inject
    OrderDAO orderDao;

    public void saveOrder(OrderDTO order, ShoppingCart cart) throws Exception {
        order.setProductList(mapProducts(cart));
        orderDao.saveOrder(order);
        cart.clear();
    }
    private List<ProductDTO> mapProducts(ShoppingCart cart){
        List<ProductDTO> products = new ArrayList<>();
        cart.getItems().forEach(item -> {
            products.add(item.getItem());
            for (int i = 1; i <= item.getQuantity(); i++) {
                products.add(item.getItem());
            }
        });
        return products;
    }
}
