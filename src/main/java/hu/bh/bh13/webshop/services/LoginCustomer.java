package hu.bh.bh13.webshop.services;

import hu.bh.bh13.webshop.daos.AccountDAO;
import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.utils.HashTool;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class LoginCustomer {

    @Inject
    AccountDAO dao;
    @Inject
    HashTool hashTool;
    @Inject
    InputValidation loginValidation;

    public AccountDTO login(AccountDTO account) {

        AccountDTO existingAccount = null;
        try {
            existingAccount = dao.findByEmail(account.getEmail());
            byte[] receivedSalt = existingAccount.getSalt();
            String password1 = hashTool.getSecurePassword(account.getPassword(), receivedSalt);
            String password2 = existingAccount.getPassword();

            if (password2.equals(password1)) {
               return existingAccount;
            }
            return existingAccount;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public boolean checkPassword(AccountDTO account, AccountDTO existingAccount) {
        byte[] receivedSalt = existingAccount.getSalt();
        String password1 = hashTool.getSecurePassword(account.getPassword(), receivedSalt);
        String password2 = existingAccount.getPassword();
        return password2.equals(password1);
    }

}
