package hu.bh.bh13.webshop.services;

import hu.bh.bh13.webshop.cart.ShoppingCart;
import hu.bh.bh13.webshop.cart.ShoppingCartItem;
import hu.bh.bh13.webshop.daos.ProductsDAO;
import hu.bh.bh13.webshop.dtos.ProductDTO;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class ShoppingCartHandler {

    @Inject
    private ProductsDAO productDao;

    public void add(long id, ShoppingCart cart) throws Exception {
        ProductDTO product = productDao.findById(id);
        if (product != null) {
            cart.add(new ShoppingCartItem(id, product));
        }
    }

    public void remove(long itemId, ShoppingCart cart) {
        cart.remove(itemId);
    }

    public double totalPrice(ShoppingCart cart) {
        return cart.getTotal();
    }

    public void resetCart(ShoppingCart cart) {
        cart.clear();
    }

    public List<ShoppingCartItem> getAllItem(ShoppingCart cart) {
        return cart.getItems();
    }

    public void checkout() {
        // wip
    }
}
