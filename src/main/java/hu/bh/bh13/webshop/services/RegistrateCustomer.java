
package hu.bh.bh13.webshop.services;

import hu.bh.bh13.webshop.daos.AccountDAO;
import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.utils.HashTool;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class RegistrateCustomer {
    @Inject
    HashTool hashTool;
    @Inject
    AccountDAO accountDao;

    public boolean occupiedEmail(String email)  {
        boolean flag = false;
        try {
            flag = accountDao.findByEmail(email) != null;
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return flag;
    }


    public boolean registrate(AccountDTO account){
        boolean success = false;
        try {
            if (!occupiedEmail(account.getEmail())) {
                account.setSalt(hashTool.getSalt());
                account.setPassword(hashTool.getSecurePassword(account.getPassword(), account.getSalt()));
                accountDao.insert(account);
                success = true;
            } 
        } catch (Exception e) {
            e.printStackTrace();
        }
       return success;
    }
}
