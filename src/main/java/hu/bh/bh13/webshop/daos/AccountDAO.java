package hu.bh.bh13.webshop.daos;

import hu.bh.bh13.webshop.dtos.AccountDTO;
import hu.bh.bh13.webshop.entities.Account;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.commons.beanutils.BeanUtils;

@Singleton
public class AccountDAO {

  
    @PersistenceContext
    private EntityManager em;

    public AccountDTO findById(long id) {
        Account acc = em.find(Account.class, id);
        AccountDTO dto = new AccountDTO();
        if (acc != null) {
            try {
                BeanUtils.copyProperties(dto, acc);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
        return dto;
    }

    public AccountDTO findByEmail(String email) {
        AccountDTO account = null;
        Query query = em.createNamedQuery("Account.findByEmail");
        query.setParameter("email", email);
        Account acc = null;
        try {
            acc = (Account) query.getSingleResult();
            account = new AccountDTO();
            BeanUtils.copyProperties(account, acc);
        } catch (Exception ex) {
            System.out.println("Didn't find an account with the given email.");
        }
        return account;
    }

    public void insert(AccountDTO account) throws Exception {
        Account accountEntity = new Account();

        try {
            BeanUtils.copyProperties(accountEntity, account);
            System.out.println("INSERT : account" + accountEntity);
            em.persist(accountEntity);
        } catch (Exception e) {
            throw new Exception("Unable to registrate the given account.");
        }
    }

    public void updateAccount(AccountDTO account) throws Exception {
        Account accountEntity = new Account();
        try {

            BeanUtils.copyProperties(accountEntity, account);
            System.out.println("DAO acount Entity" + accountEntity);
            em.merge(accountEntity);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Unable to update the given account.");
        }
    }

    public List<AccountDTO> getAllAccountDTO() {

        List<Account> accounts = new ArrayList<>();
        List<AccountDTO> accountsDto = new ArrayList<>();
        AccountDTO accDto = new AccountDTO();

        try {

            Query namedQuery = em.createNamedQuery("Accounts.findAll");

            accounts.addAll(namedQuery.getResultList());
            for (int i = 0; i < accounts.size(); i++) {
                BeanUtils.copyProperties(accDto, accounts.get(i));
                accountsDto.add(accDto);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return accountsDto;

    }

    public boolean deleteAccount(long id) {

        try {
            em.remove(em.find(Account.class, id));
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
