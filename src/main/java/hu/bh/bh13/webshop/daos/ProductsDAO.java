package hu.bh.bh13.webshop.daos;

import hu.bh.bh13.webshop.dtos.ProductDTO;
import hu.bh.bh13.webshop.entities.Product;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Singleton
public class ProductsDAO {

    @PersistenceContext
    private EntityManager em;

    
    public void insert(ProductDTO productDto) throws Exception {
        try {
            em.persist(productDto);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Unable to insert product" + ex.getMessage());
        }
    }

    public ProductDTO findById(long id) {
        Product pr = em.find(Product.class, id);
        ProductDTO dto = null;
        if (pr != null) {
            dto = new ProductDTO.Builder().setName(pr.getName())
                    .setManufacturer(pr.getManufacturer())
                    .setOnSale(pr.getOnSale())
                    .setPrice(pr.getPrice())
                    .setProductId(pr.getProductId())
                    .setOrderId(pr.getOrderId())
                    .setDetails(pr.getDetails())
                    .setImgpath(pr.getProductimage().getImgpath())
                    .buildwithId();
        } 
        return dto;
    }

    public List<ProductDTO> getAllProducts() {

        List<ProductDTO> products = new ArrayList<>();
        try {
            Query namedQuery = em.createNamedQuery("Product.findAll");
            List<Product> productEntities = namedQuery.getResultList();

            for (Product pr : productEntities) {
                products.add(new ProductDTO.Builder().setName(pr.getName())
                        .setManufacturer(pr.getManufacturer())
                        .setOnSale(pr.getOnSale())
                        .setPrice(pr.getPrice())
                        .setProductId(pr.getProductId())
                        .setOrderId(pr.getOrderId())
                        .setDetails(pr.getDetails())
                        .setImgpath(pr.getProductimage().getImgpath())
                        .buildwithId());
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return products;
    }

    public List<ProductDTO> findByManufacturer(String manu){
        List<ProductDTO> manulist = new ArrayList<>();
        try {
            Query man = em.createNamedQuery("Product.findByManufacturer");
            man.setParameter("manufacturer", manu);
            List<Product> pr = man.getResultList();
            for (Product product : pr) {
                ProductDTO dto = new ProductDTO.Builder().setName(product.getName())
                        .setManufacturer(product.getManufacturer())
                        .setOnSale(product.getOnSale())
                        .setPrice(product.getPrice())
                        .setProductId(product.getProductId())
                        .setOrderId(product.getOrderId())
                        .setDetails(product.getDetails())
                        .setImgpath(product.getProductimage().getImgpath())
                        .buildwithId();
                manulist.add(dto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return manulist;
    }

    public List<ProductDTO> findByName(String name){
        List<ProductDTO> namelist = new ArrayList<>();

        try {
            Query result = em.createNamedQuery("Product.findByName");
            result.setParameter("name", name);
            List<Product> pr = result.getResultList();

            for (Product product : pr) {
                ProductDTO dto = new ProductDTO.Builder().setName(product.getName())
                        .setManufacturer(product.getManufacturer())
                        .setOnSale(product.getOnSale())
                        .setPrice(product.getPrice())
                        .setProductId(product.getProductId())
                        .setOrderId(product.getOrderId())
                        .setDetails(product.getDetails())
                        .setImgpath(product.getProductimage().getImgpath())
                        .buildwithId();
                namelist.add(dto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return namelist;
    }
        public List<ProductDTO> findByNameContains(String name){
        List<ProductDTO> namelist = new ArrayList<>();

        try {
            List<ProductDTO> allProduct = getAllProducts();
            for (ProductDTO productDTO : allProduct) {
                if(productDTO.getName().toLowerCase().contains(name.toLowerCase())) {
                   namelist.add(productDTO);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return namelist;
    }

    public List<ProductDTO> findBySale() {
        List<ProductDTO> salelist = new ArrayList<>();

        try {
            Query result = em.createNamedQuery("Product.findByOnSale");
            result.setParameter("onSale", true);
            List<Product> pr = result.getResultList();

            for (Product product : pr) {
                ProductDTO dto = new ProductDTO.Builder().setName(product.getName())
                        .setManufacturer(product.getManufacturer())
                        .setOnSale(product.getOnSale())
                        .setPrice(product.getPrice())
                        .setProductId(product.getProductId())
                        .setOrderId(product.getOrderId())
                        .setDetails(product.getDetails())
                        .setImgpath(product.getProductimage().getImgpath())
                        .buildwithId();
                salelist.add(dto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return salelist;
    }
    
        public void removeById(long id) throws Exception{
        try{
            Product pr = em.find(Product.class, id);
            em.getTransaction().begin();
            em.remove(pr);
            em.getTransaction().commit();
         } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Unable to remove product" + ex.getMessage());
        } finally {
            if (em == null) {
                em.close();
            }   
        }
        }

}
