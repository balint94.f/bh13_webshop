package hu.bh.bh13.webshop.daos;

import hu.bh.bh13.webshop.dtos.OrderDTO;
import hu.bh.bh13.webshop.dtos.ProductDTO;
import hu.bh.bh13.webshop.entities.Account;
import hu.bh.bh13.webshop.entities.OrderE;
import hu.bh.bh13.webshop.entities.Product;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.beanutils.BeanUtils;

@Singleton
public class OrderDAO {

    @PersistenceContext
    private EntityManager em;

    public void saveOrder(OrderDTO order) throws Exception{
        List<OrderE> ordersEntity = new ArrayList<>();
        
        BeanUtils.copyProperties(ordersEntity, order.getAccount().getOrders());
        Account account = new Account(
                order.getAccount().getId(), 
                order.getAccount().getFirstName(), 
                order.getAccount().getLastName(), 
                order.getAccount().getEmail(), 
                order.getAccount().getAccountRole(), 
                order.getAccount().getPassword(), 
                order.getAccount().getSalt(), 
                ordersEntity);
        
        List<ProductDTO> products = order.getProductList();
        List<Product> productEntities = new ArrayList();
        
        products.forEach(item -> {
            Product product = new Product(item.getProductimage(), 
                    item.getProductId(), 
                    item.getName(), 
                    item.getPrice(), 
                    item.getManufacturer(), 
                    item.getOnSale(), 
                    item.getOrderId(),
                    item.getDetails());
            
            productEntities.add(product);
        });
        OrderE newOrder = new OrderE( 
                order.getOrderId(),
                account,
                order.getDateOfOrder(),
                productEntities,
                order.getAddress(),
                order.getCountry(),
                order.getState(),
                order.getZipCode()
        );
        try {
            em.persist(newOrder);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Unable to save the new order, id: " + order.getOrderId());
        }
    }
}
