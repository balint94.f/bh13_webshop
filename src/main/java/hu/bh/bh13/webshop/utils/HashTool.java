
package hu.bh.bh13.webshop.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.ejb.Stateless;

@Stateless
public class HashTool {
    private static final String HASH_CODE = "SHA-256";
    
    public String getSecurePassword(String password, byte[] salt) {

        String securedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance(HASH_CODE);
            md.update(salt);
            byte[] bytes = md.digest(password.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            securedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return securedPassword;
    }
    
    public byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return salt;
    }

}
