
package hu.bh.bh13.webshop.utils;

import javax.ejb.Stateless;

@Stateless
public class RandomPasswordGenerator {

    //'0'-'9' => 48-57 in ASCII
    //'A'-'Z' => 65-90 in ASCII
    //'a'-'z' => 97-122 in ASCII
    private static final int PASSWORD_LENGTH = 10;
    
    public String generateRandomPassword() {
        String randomPassword = "";
        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            randomPassword += generateRandomChar();
        }
        return randomPassword;
    }
    public char generateRandomChar() {
        //Generate a random number that represents all possible charachters in our password
        //10 digits, 26 uppercase letters, 26 lowercase letters = 62 possible charachters
        int random = (int)(Math.random() * 62);
        //Break up random into intervals to represent numbers, uppercase and lowercase letters
        //random is 0 to 61 inclusive
        //if random is between 0-9 => number
        //if random is between 10-35 => uppercase
        //if random is between 36-61 => lowecase
        if(random <= 9) {
            int ascii = random + 48;
            return (char)ascii;
        } else if(random <= 35) {
            int ascii = random + 55;
            return (char)ascii;
        } else {
            int ascii = random + 61;
            return (char)ascii;
        }
    }
}
