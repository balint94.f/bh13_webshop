/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.bh13.webshop.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Balint
 */
@Entity
@Table(name = "productimage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Productimage.findAll", query = "SELECT p FROM Productimage p"),
    @NamedQuery(name = "Productimage.findByImageId", query = "SELECT p FROM Productimage p WHERE p.imageId = :imageId"),
    @NamedQuery(name = "Productimage.findByImgpath", query = "SELECT p FROM Productimage p WHERE p.imgpath = :imgpath")})
public class Productimage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "imageId")
    private Long imageId;
    @Size(max = 200)
    @Column(name = "imgpath")
    private String imgpath;
    @JoinColumn(name = "imageId", referencedColumnName = "productId", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Product product;

    public Productimage() {
    }

    public Productimage(Long imageId) {
        this.imageId = imageId;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public String getImgpath() {
        return imgpath;
    }

    public void setImgpath(String imgpath) {
        this.imgpath = imgpath;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imageId != null ? imageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Productimage)) {
            return false;
        }
        Productimage other = (Productimage) object;
        if ((this.imageId == null && other.imageId != null) || (this.imageId != null && !this.imageId.equals(other.imageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "hu.bh.bh13.webshop.entities.Productimage[ imageId=" + imageId + " ]";
    }
    
}
