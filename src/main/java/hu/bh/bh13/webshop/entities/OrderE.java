package hu.bh.bh13.webshop.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "orders")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderE.findAll", query = "SELECT o FROM OrderE o"),
    @NamedQuery(name = "OrderE.findByOrderId", query = "SELECT o FROM OrderE o WHERE o.orderId = :orderId"),
    @NamedQuery(name = "OrderE.findByUserId", query = "SELECT o FROM OrderE o WHERE o.account.id = :userId"),
    @NamedQuery(name = "OrderE.findByDateOfOrder", query = "SELECT o FROM OrderE o WHERE o.dateOfOrder = :dateOfOrder")})
public class OrderE implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "orderId")
    private Long orderId;

    @ManyToOne
    @JoinColumn(name = "userId")
    private Account account;

    @Column(name = "dateOfOrder")
    @Temporal(TemporalType.DATE)
    private Date dateOfOrder;
    @OneToMany(mappedBy = "orderId")
    private List<Product> productList;

    @Column(name = "address")
    private String address;

    @Column(name = "country")
    private String country;

    @Column(name = "state")
    private String state;

    @Column(name = "zip")
    private int zipCode;

    public OrderE() {
    }

    public OrderE(Long orderId) {
        this.orderId = orderId;
    }

    public OrderE(Long orderId, Account account, Date dateOfOrder, List<Product> productList) {
        this.orderId = orderId;
        this.account = account;
        this.dateOfOrder = dateOfOrder;
        this.productList = productList;
    }

    public OrderE(Long orderId, Account account, Date dateOfOrder, List<Product> productList, String address, String country, String state, int zipCode) {
        this.orderId = orderId;
        this.account = account;
        this.dateOfOrder = dateOfOrder;
        this.productList = productList;
        this.address = address;
        this.country = country;
        this.state = state;
        this.zipCode = zipCode;
    }
    

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Date getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(Date dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    @XmlTransient
    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderId != null ? orderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderE)) {
            return false;
        }
        OrderE other = (OrderE) object;
        if ((this.orderId == null && other.orderId != null) || (this.orderId != null && !this.orderId.equals(other.orderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OrderE{" + "orderId=" + orderId + ", account=" + account + ", dateOfOrder=" + dateOfOrder + ", productList=" + productList + ", address=" + address + ", country=" + country + ", state=" + state + ", zipCode=" + zipCode + '}';
    }

}
